package cpw.mods.fml.relauncher.addon;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ModList {

    private HashMap<String, ModData> tweakers = new HashMap<>();
    private List<File> cachedModFiles;

    private final File tweakersPath = new File("." + File.separator + "tweakers.txt");
    private final File tweakersAccessPath = new File("." + File.separator + "tweakers.access");

    public boolean init(File[] modList) {
        cachedModFiles = new ArrayList(Arrays.asList(modList));

        //read tweakers.access
        try {
            BufferedReader tweakersAccessFile = new BufferedReader(new FileReader(tweakersAccessPath));
            LogManager.getLogger(this).log(Level.INFO, "File \"tweakers.access\" founded!");

            boolean run = Boolean.parseBoolean(tweakersAccessFile.readLine());

            tweakersAccessFile.close();

            if (!run) return false;
        } catch (IOException e) {
            LogManager.getLogger(this).log(Level.ERROR, "File \"tweakers.access\" not found! Create new empty file.");
            try {
                tweakersAccessPath.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        // read tweakers.txt
        try {
            BufferedReader tweakersFile = new BufferedReader(new FileReader(tweakersPath));
            LogManager.getLogger(this).log(Level.INFO, "File \"tweakers.txt\" founded!");

            tweakersFile.lines().forEach(s -> {
                String[] data = s.split(";");
                if (data.length >= 3)
                    tweakers.put(data[0].trim(), new ModData(data[0].trim(), data[1].trim(), Boolean.parseBoolean(data[2]), data[3].trim()));
            });

            tweakersFile.close();
        } catch (IOException e) {
            LogManager.getLogger(this).log(Level.ERROR, "File \"tweakers.txt\" not found! Create new empty file.");
            try {
                tweakersPath.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return true;
    }

    public boolean modIsEnabled(String modName) {
        return tweakers.containsKey(modName) ? tweakers.get(modName).isActive() : true;
    }

    public File[] getEnabledMods() {
    	//cachedModFiles.forEach(s -> System.out.println(s.getName()));
        for (Map.Entry<String, ModData> entry : tweakers.entrySet()) {
            Iterator<File> iterator = cachedModFiles.iterator();

            while (iterator.hasNext()) {
                File mod = iterator.next();

                if (mod.getName().equals(entry.getKey())) {
                    //System.out.println(mod.getName());

                    if (!entry.getValue().isActive()) {
                        iterator.remove();
                    }
                }
            }
        }

        return cachedModFiles.toArray(new File[cachedModFiles.size()]);
    }

    public void write(){
        List<String> toWrite = new ArrayList<>();

        for(ModData mod: tweakers.values()) {
            toWrite.add(mod.toSourceString());
        }
        try {
            FileUtils.writeLines(tweakersPath, toWrite);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean containsMod(String name){
        return tweakers.containsKey(name);
    }

    public ModData getMod(String name){
        return tweakers.get(name);
    }

}
