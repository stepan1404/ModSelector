package cpw.mods.fml.relauncher.addon;

import akka.japi.pf.FI;

import java.io.File;

public class ModData {

    private String file;
    private String name;
    private boolean isActive;
    private String desc;

    public ModData(String file, String name, boolean isActive, String desc) {
        this.file = file;
        this.name = name;
        this.isActive = isActive;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getFile() {
        return file;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean value) {
        this.isActive = value;
    }

    public String getDesc() {
        return this.desc;
    }

    public String toSourceString() {
        return this.file + ";" + this.name + ";" + this.isActive + ";" + this.desc;
    }
}
