package cpw.mods.fml.relauncher.addon;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cpw.mods.fml.relauncher.CoreModManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.net.URL;

public class ModSelectorView {

	private JFrame frame;
	private JList list;
	private JPanel panel;
	private JPanel panel_1;
	private JToggleButton toggler;
	private JButton launch;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;

    private ModSelectorController controller;
    private ModList model;
    
    private JLabel modName;
    private JLabel timerLabel;
    private int timer = 40;
    private JTextArea modDesc;
	private JPanel panel1;
    private JTextPane textPane1;
    
    private boolean isSleep = true;
    
    private final String project = "" + (char)98 + (char)111 + (char)98 + (char)114 + (char)45 + (char)99 + (char)114 + 
    		(char)97 + (char)102 + (char)116 + (char)46 + (char)114 + (char)117;

    private Color green = new Color(27, 94, 32);
    

    public ModSelectorView(ModSelectorController controller) {
		this.controller = controller;
		this.model = controller.getModList();
		
		
		SwingUtilities.invokeLater(new Runnable() {		
				public void run() {
					initialize();
					
					new Thread("Timer \"Auto-Closing\"") {
						
						private long startTime = System.currentTimeMillis() / 1000;
						private long currentTime = startTime;
						
						public void run() {
							while(currentTime != startTime + timer && isSleep) {
								currentTime = System.currentTimeMillis() / 1000;
								timerLabel.setText("" + (startTime + timer - currentTime));
							}
							
							if(isSleep) {
								CoreModManager.getWaiter().countDown();
					            frame.dispose();
							} else {
								timerLabel.setText("");
							}
						}
						
					}.start();
				}
		});
				
	}

	private void initialize() {
		frame = new JFrame("\u0412\u044b\u0431\u043e\u0440 \u043c\u043e\u0434\u043e\u0432 \u007c " + project);
		frame.setBounds(100, 100, 348, 306);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		URL iconURL = getClass().getResource("favicon.png");
		ImageIcon icon = null;
		
		if(iconURL != null)
			icon = new ImageIcon(iconURL);
		
		if(icon != null)
			frame.setIconImage(icon.getImage());

		panel = new JPanel();
		panel.setBounds(109, 0, 233, 246);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel.setBounds(10, 11, 70, 14);
		panel.add(lblNewLabel);
		
		modName = new JLabel("Name");
		modName.setFont(new Font("Tahoma", Font.PLAIN, 11));
		modName.setVerticalAlignment(SwingConstants.TOP);
		modName.setBounds(73, 11, 137, 35);
		panel.add(modName);
		
		JLabel lblNewLabel_2 = new JLabel("\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(10, 51, 58, 14);
		panel.add(lblNewLabel_2);
		
		timerLabel = new JLabel("40", SwingConstants.CENTER);
		timerLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		timerLabel.setBounds(140, 221, 70, 25);
		panel.add(timerLabel);
		
		JTextArea modDesc = new JTextArea();
		modDesc.setFont(new Font("Tahoma", Font.PLAIN, 11));
		modDesc.setEditable(false);
		modDesc.setText("Desc");
		modDesc.setBackground(UIManager.getColor("Button.background"));
		modDesc.setLineWrap(true);
		modDesc.setBounds(73, 51, 137, 112);
		panel.add(modDesc);
		
		panel_1 = new JPanel();
		panel_1.setBounds(109, 244, 233, 33);
		panel_1.setBorder(null);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		panel_3 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_3.getLayout();
		panel_1.add(panel_3);
		
		toggler = new JToggleButton("Toggle");
		
		toggler.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				awakeApp();
				
				ModData mod = (ModData) list.getSelectedValue();

				mod.setActive(toggler.isSelected());

				if (!mod.isActive()) {
					toggler.setForeground(Color.red);
					toggler.setText("\u0412\u044b\u043a\u043b\u044e\u0447\u0435\u043d");
				} else {
					toggler.setForeground(green);
					toggler.setText("\u0412\u043a\u043b\u044e\u0447\u0435\u043d");
				}

				SwingUtilities.updateComponentTreeUI(list);
			}

		});
		
		panel_3.add(toggler);
		
		panel_4 = new JPanel();
		panel_1.add(panel_4);
		
		launch = new JButton("\u0418\u0433\u0440\u0430\u0442\u044c");
		
		launch.addActionListener(arg0 -> {

            model.write();
            CoreModManager.getWaiter().countDown();
            frame.dispose();

        });
		
		panel_4.add(launch);
		
		panel_2 = new JPanel();
		panel_2.setBounds(0, 0, 109, 277);
		panel_2.setBorder(new LineBorder(new Color(0, 51, 51)));
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(new GridLayout(1, 1, 0, 0));
		
		list = new JList() {
			public String getToolTipText(MouseEvent evt) {
		        int index = locationToIndex(evt.getPoint());
		        
		        Object item = getModel().getElementAt(index);

		        return item.toString();
		      }
		};
		
		list.setBorder(new TitledBorder(new EmptyBorder(5, 0, 0, 0), "\u041C\u043E\u0434\u044B", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(list);
		
		panel_2.add(scrollPane);
		
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setModel(controller.getMods());
		
		
		list.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        private boolean isFirst = true;

			public void valueChanged(ListSelectionEvent event) {
	        	
	        	if(!isFirst) {
	        		awakeApp();
	        	} else {
	        		isFirst = false;
	        	}

	        	ModData mod = (ModData) list.getSelectedValue();
	        	
	            modName.setText(mod.toString());
	            modDesc.setText(mod.getDesc());
	            toggler.setSelected(mod.isActive());
	            
	            if (!mod.isActive()) {
					toggler.setForeground(Color.red);
					toggler.setText("\u0412\u044b\u043a\u043b\u044e\u0447\u0435\u043d");
				} else {
					toggler.setForeground(green);
					toggler.setText("\u0412\u043a\u043b\u044e\u0447\u0435\u043d");
				}
	            
	        }
	    });
		list.setCellRenderer(new ListRenderer());
		list.setSelectedIndex(0);
		
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	private void awakeApp() {
		this.isSleep = false;	
	}

	private class ListRenderer extends DefaultListCellRenderer
    {
  
        public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            ModData mod = (ModData) value;

			if (!mod.isActive()) {
				setForeground(Color.red);
			} else {
				setForeground(green);
			}

			return (this);
		}
    }
}
