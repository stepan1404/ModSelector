package cpw.mods.fml.relauncher.addon;

import javax.swing.*;

import cpw.mods.fml.relauncher.CoreModManager;

import java.io.File;

public class ModSelectorController {
    private static DefaultListModel<ModData> mods = new DefaultListModel<ModData>();
    private static ModList modList;
    private ModSelectorView view;

    public ModSelectorController(File[] list) {
        this.modList = new ModList();
        
        if(setModsList(list))        
        	this.view = new ModSelectorView(this);
        else
        	CoreModManager.getWaiter().countDown();
    }

    private boolean setModsList(File[] list){
        mods.clear();

        boolean flag = modList.init(list);

        for (File f : list){
            if(modList.containsMod(f.getName())) {
                mods.addElement(modList.getMod(f.getName()));
            }
        }
        
        return flag;
    }

    public DefaultListModel getMods(){
        return mods;
    }

    public ModList getModList() {
        return modList;
    }

    public static boolean modIsEnabled(String modName) {
        return modList.modIsEnabled(modName);
    }
}
